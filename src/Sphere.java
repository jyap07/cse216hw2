public class Sphere implements ThreeDShape {
    private ThreeDPoint center;
    private double radius;

    public double getRadius(){
        return radius;
    }

    public Sphere(double centerX, double centerY, double centerZ,
                  double radius) {
        center = new ThreeDPoint(centerX, centerY, centerZ);;
        this.radius = radius;
    }

    public ThreeDPoint center() {
        return center;
    }

    @Override
    public double volume() {
        double num = 4.0/3 * Math.PI * Math.pow(this.radius, 3);
        return (double) Math.round(num * 10000) / 10000;
    }

    @Override
    public int compareTo(ThreeDShape threeDShape) {
        if (this.volume() > threeDShape.volume()) {
            return 1;
        }
        else if (this.volume() < threeDShape.volume()) {
            return -1;
        }
        else {
            return 0;
        }
    }
    public static Sphere random() {
        double x = Math.random() * 200 - 100;
        double y = Math.random() * 200 - 100;
        double z = Math.random() * 200 - 100;
        double radius = Math.random() * 100;
        return new Sphere(x, y, z, radius);
    }

    public double surfaceArea() {
        double num = Math.pow(this.radius, 2) * Math.PI * 4;
        return (double) Math.round(num * 10000) / 10000;
    }
}
