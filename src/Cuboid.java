import java.util.ArrayList;
import java.util.List;

public class Cuboid implements ThreeDShape {

    private final ThreeDPoint[] vertices = new ThreeDPoint[8];

    public ThreeDPoint[] getVertices() {
        return vertices;
    }

    /**
     * Creates a cuboid out of the list of vertices. It is expected that the vertices are provided in
     * the order as shown in the figure given in the homework document (from v0 to v7).
     * 
     * @param vertices the specified list of vertices in three-dimensional space.
     */
    public Cuboid(List<ThreeDPoint> vertices) {
        if (vertices.size() != 8)
            throw new IllegalArgumentException(String.format("Invalid set of vertices specified for %s",
                                                             this.getClass().getName()));
        int n = 0;
        for (ThreeDPoint p : vertices) this.vertices[n++] = p;
    }

    public double distance(ThreeDPoint p1, ThreeDPoint p2) {
        double xSquare = Math.pow(p2.getX() - p1.getX(), 2);
        double ySquare = Math.pow(p1.getY() - p2.getY(), 2);
        double zSquare = Math.pow(p1.getZ() - p2.getZ(), 2);
        return Math.sqrt(xSquare + ySquare + zSquare);
    }
    @Override
    public double volume() {
        double num = distance(vertices[2], vertices[3]) * distance(vertices[3],
                vertices[4]) * distance(vertices[4], vertices[5]);
        return (double) Math.round(num * 10000) / 10000;

    }
    @Override
    public ThreeDPoint center() {
        return new ThreeDPoint((vertices[3].getX() + vertices[2].getX())/2,
                (vertices[1].getY() + vertices[2].getY())/2,
                (vertices[5].getZ() + vertices[0].getZ())/2);
    }

    @Override
    public int compareTo(ThreeDShape threeDShape) {
       if (this.volume() > threeDShape.volume()) {
            return 1;
        }
       else if (this.volume() < threeDShape.volume()) {
           return -1;
        }
       else return 0;
    }

    public static Cuboid random() {
       double z1 = (double) Math.round(Math.random() * 2000000)/10000 - 100;
       double z2 = (double) Math.round(Math.random() * 2000000)/10000 - 100;
       double x1 = (double) Math.round(Math.random() * 2000000)/10000 - 100;
       double x2 = (double) Math.round(Math.random() * 2000000)/10000 - 100;
       double y1 = (double) Math.round(Math.random() * 2000000)/10000 - 100;
       double y2 = (double) Math.round(Math.random() * 2000000)/10000 - 100;
       ThreeDPoint[] aVertices = new ThreeDPoint[8];
       for (int i = 0; i < aVertices.length; i++) {
           aVertices[i] = new ThreeDPoint();
       }
       for (int i = 0; i < aVertices.length; i++) {
           if (i <= 3 ) {
               aVertices[i].setZ(Math.min(z1, z2));
           }
           else {
               aVertices[i].setZ(Math.max(z1, z2));
           }
           if (i == 0 || (i >= 3 && i <= 5)) {
               aVertices[i].setX(Math.max(x1, x2));
           }
           else {
               aVertices[i].setX(Math.min(x1, x2));
           }
           if (i == 7 || (i >= 2 && i <= 4)) {
               aVertices[i].setY(Math.min(y1, y2));
           }
           else {
               aVertices[i].setY(Math.max(y1, y2));
           }
       }
       List<ThreeDPoint> rVertices = new ArrayList<ThreeDPoint>();
       for (ThreeDPoint pt : aVertices) {
           rVertices.add(pt);
       }
       return new Cuboid(rVertices);
    }
    public double surfaceArea() {
        double face1Area =
                distance(vertices[0], vertices[1]) * distance(vertices[1],
                vertices[2]) * 2;
        double face2Area =
                distance(vertices[0], vertices[1]) * distance(vertices[0],
                vertices[5]) * 2;
        double face3Area =
                distance(vertices[0], vertices[5]) * distance(vertices[0],
                        vertices[3]) * 2;
        return (double) Math.round( (face1Area + face2Area + face3Area) * 10000) / 10000;
    }

}
