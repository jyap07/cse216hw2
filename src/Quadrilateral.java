import java.util.Arrays;
import java.util.List;

public class Quadrilateral implements Positionable, TwoDShape {

    private final TwoDPoint[] vertices = new TwoDPoint[4];

    public TwoDPoint[] getVertices() {
        return vertices;
    }

    public Quadrilateral() {
    }
    public Quadrilateral(double[] vertices) {
        this(TwoDPoint.ofDoubles(vertices));
    }

    public Quadrilateral(List<TwoDPoint> vertices) {
        int n = 0;
        for (TwoDPoint p : vertices) this.vertices[n++] = p;
        if (!isMember(vertices))
            throw new IllegalArgumentException(String.format("Invalid set of vertices specified for %s",
                                                             this.getClass().getCanonicalName()));
    }

    /**
     * Given a list of four points, adds them as the four vertices of this quadrilateral in the order provided in the
     * list. This is expected to be a counterclockwise order of the four corners.
     *
     * @param points the specified list of points.
     * @throws IllegalStateException if the number of vertices provided as input is not equal to four.
     */
    @Override
    public void setPosition(List<? extends Point> points) {
        for (int i = 0; i < 4; i++) {
            vertices[i] = (TwoDPoint) points.get(i);
        }
    }

    @Override
    public List<TwoDPoint> getPosition() {

        return Arrays.asList(vertices);
    }

    public double distance(TwoDPoint p1, TwoDPoint p2) {
        double xSquare = Math.pow(p1.getX() - p2.getX(), 2);
        double ySquare = Math.pow(p1.getY() - p2.getY(), 2);
        return Math.sqrt(xSquare + ySquare);
    }

    /**
     * @return the lengths of the four sides of the quadrilateral. Since the setter {@link Quadrilateral#setPosition(List)}
     *         expected the corners to be provided in a counterclockwise order, the side lengths are expected to be in
     *         that same order.
     */
    protected double[] getSideLengths() {
        double[] distances = new double[4];
        for (int i = 0; i < 4; i++) {
            distances[i] = distance(vertices[i], vertices[(i + 1)  % 4]); }
        return distances;
    }

    public double getSmallestX() {
        double min = this.vertices[0].getX();
        for (TwoDPoint p : vertices) {
            if (p.getX() < min) {
                min = p.getX();
            }
        }
        return min;
    }

    @Override
    public int numSides() { return 4; }

    @Override
    public boolean isMember(List<? extends Point> vertices) { return vertices.size() == 4; }
}
