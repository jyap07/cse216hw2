import java.util.List;

public class Rectangle extends Quadrilateral implements SymmetricTwoDShape {
    final static double EPSILON = .0000000001;

    public Rectangle(List<TwoDPoint> vertices) {
        super(vertices);
    }
    /**
     * The center of a rectangle is calculated to be the point of intersection of its diagonals.
     *
     * @return the center of this rectangle.
     */
    @Override
    public Point center() {
        return new TwoDPoint((this.getVertices()[0].getX() +
            this.getVertices()[2].getX()) / 2, (this.getVertices()[0].getY() +
            this.getVertices()[2].getY())/2);
    }

    public double findDistance(Point a, Point b) {
        return Math.sqrt(Math.pow(b.coordinates()[0] - a.coordinates()[0], 2)
            + Math.pow(b.coordinates()[1] - a.coordinates()[1], 2));
    }

    @Override
    public boolean isMember(List<? extends Point> vertices) {
        if (super.isMember(vertices)) {
            if (Math.abs(findDistance(vertices.get(0), vertices.get(1)) -
                    findDistance(vertices.get(2), vertices.get(3))) < EPSILON &&
                    Math.abs(findDistance(vertices.get(1), vertices.get(2)) -
                            findDistance(vertices.get(3), vertices.get(0))) < EPSILON &&
                    Math.abs(findDistance(vertices.get(0), vertices.get(2)) -
                            findDistance(vertices.get(1), vertices.get(3))) < EPSILON) {
                return true;
            }
            else return false;
        }
        else return false;
    }

    @Override
    public double area() {
        double num = distance(this.getVertices()[2], this.getVertices()[3]) *
            distance(this.getVertices()[3], this.getVertices()[0]);
        return (double) Math.round(num * 10000) / 10000;
    }
}
