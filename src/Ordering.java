import java.util.*;

public class Ordering {

    static class XLocationComparator implements Comparator<TwoDShape> {
        @Override
        public int compare(TwoDShape o1, TwoDShape o2) {
            double a;
            double b;
            if (o1 instanceof Quadrilateral) {
                a = ((Quadrilateral) o1).getSmallestX();
            } else {
                a = ((Circle) o1).center().coordinates()[0] - ((Circle) o1).getRadius();
            }
            if (o2 instanceof Quadrilateral) {
                b = ((Quadrilateral) o2).getSmallestX();
            } else {
                b = ((Circle) o2).center().coordinates()[0] - ((Circle) o2).getRadius();
            }
            return (int) (a - b);
        }
    }

    static class AreaComparator implements Comparator<SymmetricTwoDShape> {
        @Override
        public int compare(SymmetricTwoDShape o1, SymmetricTwoDShape o2) {
            return (int) Math.round(o1.area() - o2.area());
        }
    }

    static class SurfaceAreaComparator implements Comparator<ThreeDShape> {
        @Override
        public int compare(ThreeDShape o1, ThreeDShape o2) {
            double a;
            double b;
            if (o1 instanceof Cuboid) {
                a = ((Cuboid) o1).surfaceArea();
            } else {
                a = ((Sphere) o1).surfaceArea();
            }
            if (o2 instanceof Cuboid) {
                b = ((Cuboid) o2).surfaceArea();
            } else {
                b = ((Sphere) o2).surfaceArea();
            }
            return (int) (a - b);
        }
    }

    static <A> void copy(Collection<? extends A> source,
                         Collection<A> destination) {
        destination.addAll(source);
    }

    public static void main(String[] args) {

        List<TwoDShape>          shapes          = new ArrayList<>();
        List<SymmetricTwoDShape> symmetricshapes = new ArrayList<>();
        List<ThreeDShape>        threedshapes    = new ArrayList<>();

        List<TwoDPoint> a = new ArrayList<TwoDPoint>();
        a.add(new TwoDPoint(10, 10));
        a.add(new TwoDPoint(0, 10));
        a.add(new TwoDPoint(0, 0));
        a.add(new TwoDPoint(10, 0));

        symmetricshapes.add(new Rectangle(a));
        symmetricshapes.add(new Square(a));
        symmetricshapes.add(new Circle(10, 10, 10));
        copy(symmetricshapes, shapes); // note-1 //
        shapes.add(new Quadrilateral(a));

        shapes.sort(new XLocationComparator());
        symmetricshapes.sort(new XLocationComparator());
        symmetricshapes.sort(new AreaComparator());

        Collections.sort(threedshapes);
        threedshapes.sort(new SurfaceAreaComparator());

        List<Number> numbers = new ArrayList<>();
        List<Double> doubles = new ArrayList<>();
        Set<Square>        squares = new HashSet<>();
        Set<Quadrilateral> quads   = new LinkedHashSet<>();

        copy(doubles, numbers); // note-2 //
        copy(squares, quads);   // note-3 //

    }
}


